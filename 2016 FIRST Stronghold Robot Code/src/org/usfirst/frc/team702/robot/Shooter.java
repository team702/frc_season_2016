package org.usfirst.frc.team702.robot;

public class Shooter {

	public enum ShootState
	{
		STANDBY, // Does nothing
		STARTUP, // Initializes, disables driving and just focuses on shooting
		LOCK,
		FLYWHEEL, // Phase 1 - Spins up the flywheels to get max velocity
		COLLECTOR, // Phase 2 - Spins collector to bring ball into collector (Flywheels still spinning)
		SHUTDOWN // Shuts down Flywheels and collector, and reinstates driver control
	}
	
	public static final double max_time_STANDBY = Double.POSITIVE_INFINITY;
	public static final double max_time_STARTUP = 2;
	public static final double max_time_LOCK = 5;
	public static final double max_time_FLYWHEEEL = 1;
	public static final double max_time_COLLECTOR = 4;
	public static final double max_time_SHUTDOWN = 1;
	
	
	public Shooter() {
		// TODO Auto-generated constructor stub
	}

}
