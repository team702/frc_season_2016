Getting Started Resources

- Software to Install
  + Eclipse IDE
  + Visual Studio Community 2015
  + 

- Software Development Resources
  + FRC Java Programming
    https://wpilib.screenstepslive.com/s/4485/m/13809
  + Java Primitive Data Types
  	https://en.wikibooks.org/wiki/Java_Programming/Primitive_Types
  + Debugging in Eclipse
    https://wpilib.screenstepslive.com/s/4485/m/23353/l/482129-debugging-simulation

- I2C Interface
 + Sending Data from a cRIO to an Arduino
   http://wpilib.screenstepslive.com/s/3120/m/7912/l/175524-sending-data-from-the-crio-to-an-arduino


- Roborio
  + Manual
    http://www.ni.com/pdf/manuals/375274a.pdf

- Ardunio
  + Programming Reference
    https://www.arduino.cc/en/Reference/HomePage
  + Uno Pin Diagram
    http://foros.giltesa.com/otros/arduino/fc/docs/pinout/uno.jpg

- Pixy
  + Wiki
    http://cmucam.org/projects/cmucam5/wiki
  + Hooking up Pixy to Arduino
    http://cmucam.org/projects/cmucam5/wiki/Hooking_up_Pixy_to_a_Microcontroller_like_an_Arduino
  + I2C Java Code Example
  	http://www.cmucam.org/boards/9/topics/3120?r=3181

- Lidar Distance Sensor
  + Github Code Link
    https://github.com/pulsedlight3d
  + Basics
    https://github.com/PulsedLight3D/LIDARLite_Basics
  + State Machine
    https://github.com/PulsedLight3D/LIDARLite_StateMachine
    
- CANBUS on Roborio Webdash
  + When instantiating new CANTalons make sure your give every new Talon an ID. Then refresh the webdash, another CANTalon will be 
  	shown. 