Welcome to the BitBucket for Team 702 "The Bagel Bytes" for the 2016 FRC Stronghold competition.

Here you can see past, and current, iterations of the robot code we are using this year.

This is run by the programming department of the team.

***Team that worked on this code:***


Programming Mentor - Sabri Sansoy && Scott Baron

Programming Lead - Ezra "Zelda" Reese

Programming Students - Ian "Sticker Meister" Horton and Maki Ifuku